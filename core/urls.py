from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('honey/', include('apps.honey.urls')),
    path("hoszaik/", include('apps.hoszaik.urls')),
    path("accounts/", include('apps.users.urls')),
]
