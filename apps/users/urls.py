from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *

app_name = 'user'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', login_required(logoutUsuario), name='logout')
]
