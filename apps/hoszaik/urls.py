from django.urls import path
from .views import *

app_name = 'hoszaik'

urlpatterns = [
    path('', HoszaikIndexView.as_view(), name='index')
]
